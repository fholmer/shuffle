# Shuffle

## Før installasjon

Du må registere en ny spotify app på https://developer.spotify.com/dashboard/
Du vil da få en *client_id* og en *client_secret* som du skal bruke. **I tillegg til dette** må du klikke inn på *edit settings* og legge inn **http://127.0.0.1/shuffle** under *redirect URIs*

Når dette er gjort må du lage en fil på PCen din som heter **shuffle.conf** hvor innholdet er:
```ini
[spotify]
user = dittspotifybrukernavn
client_id = dinclientid
client_secret = dinclientsecret
redirect_uri = http://127.0.0.1/shuffle
scope = playlist-modify-public playlist-modify-private user-modify-playback-state user-read-playback-state

source_playlist = spotify:user:slankbakfra:playlist:7c882341ALl16XN8ZuDaPP
target_playlist = spotify:user:slankbakfra:playlist:3gaY9lZo7E9h0oUyIF8AMz
target_offline_playlist = 0hvcyLVomdHttvfaF2zd6M
```

**NB!** Du må erstatte *source_playlist*, *target_playlist* og *target_offline_playlist* med dine egne spillelister.

Du kan [se denne videoen](https://youtu.be/dQw4w9WgXcQ) for å få hjelp til dette. Det er veldig lett. Bare finn spillelisten i spotify og høyreklikk -> share -> copy spotify uri. Du får da en link som ser slik ut: _spotify:user:dittbrukernavn:playlist:3gaY9lZo7E9h0oUyIF8AMz_.

Hvis spillelisten tilhører din spotifybruker så kan du komme unna med å bare ta med koden til høyre for **:playlist:**. *target_offline_playlist* er et eksempel på dette.

---
## Installasjon

### Linux:
Verifiser at python3 og pip3 er installert:
```bash
python3 -V
python3 -m pip -V
```
Installér **spotipy**:
```bash
sudo python3 -m pip install spotipy
```
Last ned [shuffle.py](https://gitlab.com/fholmer/shuffle/raw/master/shuffle.py) og plasser den i samme mappe som shuffle.conf

Når du senere skal kjøre skriptet gjøres det slik:
```bash
python3 shuffle.py
```


### Windows:
Hvis du ikke har python3 fra før så [last ned](https://www.python.org/downloads) og installer nyeste versjon.

Gå inn i DOS, og når du er i DOS så skriv følgende:
```cmd
py -3 -m pip install spotipy
```
GÅ UT AV DOS

Last ned [shuffle.py](https://gitlab.com/fholmer/shuffle/raw/master/shuffle.py) og plasser den i samme mappe som shuffle.conf

Når du senere skal kjøre skriptet gjøres det slik:
```cmd
py -3 shuffle.py
```
Eller du kan bare dobbeltklikke på shuffle.py

---

## Første gangs autentisering
Start shuffle.py.

Nå skjer det to ting på en gang:

* En "Connect application to your spotify account" webside åpner seg. Her må du klikke OK. Etterpå åpnes en tom webside hvor URLen begynner med **http://127.0.0.1/shuffle?code=**

* I python-scriptet står det: **Enter the URL you where redirected to:**

Nå skal du kopiere hele denne URLen og lime inn i python-scriptet og trykke enter.

## Hva nå?

Her er noen forslag:

#### Kjør skriptet fra Home-assistant.
Legg inn følgende linje i configuration.yaml
```yaml
shell_command:
  spotify_shuffle: python3 filsti/til/shuffle.py
```

#### Kjør skriptet som cronjob.
editer crontab
```bash
crontab -e
```
Legg inn linjen:
```bash
0 */1 * * * python3 filsti/til/shuffle.py
```
Nå kjører shuffle en gang i timen
