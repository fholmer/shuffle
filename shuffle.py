#! python3
import os
import sys
import time
import random
import configparser
import spotipy
import spotipy.util as util

# hensikten med dette er at vi "jobber" i samme mappe som konfig-filen
os.chdir(os.path.dirname(os.path.abspath(__file__)))

class Shuffle():
    def __init__(self, spotify, user):
        self.spotify = spotify  # referanse til rest-api
        self.uri_list = []  # her lagrer vi alle uri-ene vi har funnet
        self.user = user # brukernavnet

    def get_playlist(self, source_playlist, no_of_songs=0):
        #finner ut hvor mange sanger som er i spillelisten
        total = self.spotify.user_playlist_tracks(self.user, source_playlist, 'total')
        total = total['total']
        if no_of_songs and no_of_songs < total:
            total = no_of_songs
        # vi må hente ned maks 100 sanger om gangen.
        for offset in range(0, total, 100):
            #spillelisten
            playlist = self.spotify.user_playlist_tracks(
                self.user,
                source_playlist,
                limit=100,
                offset=offset)
            # sangene i spillelisten
            track_list = playlist['items']
            # listen med URI-ene til sangene
            self.uri_list.extend([track["track"]["uri"] for track in track_list])

    def store_playlist(self, target_playlist, no_of_songs=100):
        # vi kan bare lagre 100 sanger.
        uri_list = self.uri_list[:no_of_songs]
        # TODO: må lage en for-loop hvis vi skal lagre flere enn 100)
        # erstatt spillelisten med nye sanger
        self.spotify.user_playlist_replace_tracks(self.user, target_playlist, uri_list)

    def reorder_playlist(self, target_playlist, track_number, insert_before, track_range):
        self.spotify.user_playlist_reorder_tracks(
            self.user,
            target_playlist,
            track_number,
            insert_before,
            track_range)

    def clear(self):
        self.uri_list.clear()

    def shuffle(self):
        random.shuffle(self.uri_list)

    def reverse(self):
        self.uri_list.reverse()

    def daily_mix_reorder(self, target_playlist, item_len=100, item_range=1, threshold=400):
        total = self.spotify.user_playlist_tracks(self.user, target_playlist, 'total')
        total = total['total']

        if total < (threshold + item_len):
            return

        # Dette er den beste måten å shuffle på (hvis du hører på hver dag)
        # Optimalt sett bør kilden ha over 2000 sanger, hvor du shuffler 100 om dagen [item_len]
        # Tar tilfeldig utvalg fra nederste delen av spillelisten [threshold]
        # og flytter dem til topp. Dette betyr at en sang ikke spilles
        # igjen før den er flyttet et stykke ned i listen.

        for _ in range(item_len):
            # TODO: samle denne til en stor operasjon i stedet for mange små
            track_no = random.randint(threshold, total - item_range)
            self.reorder_playlist(target_playlist, track_no, 0, item_range)


def main(configfile=None):

    if not configfile:
        # filnavnet uten .ext
        appname = os.path.basename(__file__).split(".")[0]
        configfile = "{}.conf".format(appname)

    # les konfigfil
    config = configparser.ConfigParser()
    config.read(configfile)

    # Parameter for spotify klient
    user =            config.get("spotify", "user")
    client_id =       config.get("spotify", "client_id")
    client_secret =   config.get("spotify", "client_secret")
    redirect_uri =    config.get("spotify", "redirect_uri")
    scope =           config.get("spotify", "scope")

    # Logg inn på spotify
    token = util.prompt_for_user_token(
        user,
        scope=scope,
        client_id=client_id,
        client_secret=client_secret,
        redirect_uri=redirect_uri)

    spotify = spotipy.Spotify(auth=token)

    # referanser til spillelister
    source_playlist = config.get("spotify", "source_playlist")
    target_playlist = config.get("spotify", "target_playlist")
    target_offline =  config.get("spotify", "target_offline_playlist")

    # håper dette er selvforklarende
    sh = Shuffle(spotify, user)
    sh.daily_mix_reorder(source_playlist, 50, 2, 400)
    sh.get_playlist(source_playlist, 100)
    sh.store_playlist(target_playlist, 100)
    sh.reverse()
    sh.store_playlist(target_offline, 20)
    sh.clear()

if __name__ == '__main__':
    main()
